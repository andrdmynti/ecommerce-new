<?php

  if (isset($_GET['content'])) 
    $content = $_GET['content'];
  else 
    $content = 'index';

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Holomoc e-Commerce</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="bootstrap/fonts/font-awesome/4.5.0/css/font-awesome.min.css">
  <script src="jquery-3.3.1.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
        margin-bottom: 0;
        border-radius: 0;
        z-index: 9999;
        border: 0;
        padding: 15px;
        font-size: 15px !important;
        line-height: 1.42857143 !important;
        letter-spacing: 3px;
        border-radius: 0;
    }

    .navbar li a, .navbar .navbar-brand {
        color: #fff !important;
    }

    .navbar-nav li a:hover, .navbar-nav li.active a {
        color: #fff !important;
        background-color: #848484 !important;
    }

    .navbar-default .navbar-toggle {
        border-color: transparent;
        color: #fff !important;
    }
    
    .carousel-inner img {
        width: 100%; /* Set width to 100% */
        margin: auto;
        min-height:200px;
    }
    
    .jumbotron {
      margin-bottom: 0;
    }

    .pembungkus {
      color: rgb(34, 34, 34);
      font-family: 'Roboto', sans-serif;
      font-size: 14;
      font-weight: 700;
      margin: 0 auto 30px;
      text-align: left;
      position: relative;
      padding-top: 25px;
    }

    .pembungkus h2, .brands_products h2 {
      color: rgb(34, 34, 34);
      font-family: 'Roboto', sans-serif;
      font-size: 18px;
      font-weight: 700;
      margin: 0 auto 10px;
      text-transform: uppercase;
      position: relative;
      z-index:
    }

    .pembungkus h2:after, h2.title:after{
      content: " ";
      position: absolute;
      border: 1px solid #f5f5f5;
      bottom:8px;
      left: 0;
      width: 100%;
      height: 0;
      z-index: -1;
    }

    .pembungkus h2:before{
      content: " ";
      position: absolute;
      background: #fff;
      bottom: -6px;
      width: 130px;
      height: 30px;
      z-index: -1;
      left: 50%;
      margin-left: -65px;
    }

    /*SCROLLABLE */
    .nap {
      margin: 0 -10px;
      padding: 0 10px;
      list-style: none;
      display: flex;
      overflow-x: scroll;
      -webkit-overflow-scrolling: touch;
    }
    
    .div.scrollmenu {
      background-color: #333;
      overflow: auto;
      white-space: nowrap;
    }

    .div.scrollmenu a {
      display: inline-block;
      color: white;
      text-align: center;
      padding: 14px;
      text-decoration: none;
    }

    .div.scrollmenu a:hover {
      background-color: #777;
    }

<<<<<<< HEAD
    /* CHECKOUT CART */
    span.price {
      float: right;
      color: grey;
    }

=======
    .nap > li > a {
       padding: 14px 16px;
       display: block;
       color: rgba(255, 255, 255, 0.8);
       text-decoration: none;
       text-transform: uppercase;
       font-size: 14px;
    }

    .nap > li > a.active {
       border-bottom: 2px solid #E64A19;
       position: absolute;
       z-index: 1;
    } 

>>>>>>> 6b829152395088aaa80e2767277bda4799b1f75f

    /*.my-menu {
      margin: 0 -10px;
      padding: 0 10px;
      list-style: none;
      display: flex;
      overflow-x: scroll;
      -webkit-overflow-scrolling: touch;
    }

    .my-menu > li > a {
        padding: 14px 16px;
        display: block;
        color: rgba(255, 255, 255, 0.8);
        text-decoration: none;
        text-transform: uppercase;
        font-size: 14px;
    }

    .my-menu > li > a.active {
        border-bottom: 2px solid #E64A19;
    }   
    
    ul.my-menu > li {
        display :  inline-block;
    }
    ul.my-menu > li > ul{
        position : absolute;
    }
    ul.my-menu > li > ul li{
        display: none;
    }
    ul.my-menu li:hover > ul > li {
        display: block;
        color: #fff !important;
        background-color: #848484 !important;
    }*/
    /*akhir Scrollable*/


    /* Hide the carousel text when the screen is less than 600 pixels wide */
    @media (max-width: 600px) {
      .carousel-caption {
        display: none; 
      }
    }
    
    @media only screen and (min-width: 768px) {
      .dropdown:hover .dropdown-menu {
        display: block;
      }
    }

    /* Add a gray background color and some padding to the footer */
    footer {
      padding: 13px;
    }

  </style>
</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="index.php"><img src="images/logo.png" sizes="relative"></a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="index.php?content=shop" style="color:white">SHOP</a></li>
          <li><a href="index.php?content=service" style="color:white"> SERVICE</a></li>
          <li>
            <a href="#myModal" data-toggle="modal" type="button" style="color:white"> LOGIN </a>
          </li>
          <li><a href="login.php" style="color:white"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
        </ul>
      </div>
    </div>
  </nav>

<!-- Modal Login dan Signup-->
  <div id="tombol_login" class="modal">
    <!-- Modal content -->
      <form class="modal-content animate" action="/action_page.php">
      <ul class="nav nav-tabs">
        <li class="active" style="background-color: #f2f3f4"><a data-toggle="tab" href="#LOGIN"><font color="black">LOGIN</font></a></li>
        <li style="background-color: #f2f3f4"><a data-toggle="tab" href="#SIGNUP"><font color="black">SIGN UP</font></a></li>
      </ul>

      <!-- <div class="tab-content" style="background-color: #f2f3f4; padding-top: 2px" > -->
        <div id="LOGIN" class="tab-pane fade in active" style="background-color: #ffffff">
          <div class="col-sm-0">
            <div class="imgcontainer">
              <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
              <img src="img_avatar2.png" alt="Avatar" class="avatar">
            </div>

            <div class="container">
              <label for="uname"><b>Username</b></label><br>
              <input type="text" placeholder="Enter Username" name="uname" required>
              <br>

              <label for="psw"><b>Password</b></label><br>
              <input type="password" placeholder="Enter Password" name="psw" required>
              <br>
                
              <button type="submit" style="width:90%; padding-top:20px; padding-bottom:20px;">LOGIN</button>
            </div>
          </div>
        </div>
        <div id="SIGNUP" class="tab-pane fade" style="background-color: #ffffff;">
          <div class="col-sm-0">
            <div class="imgcontainer">
              <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
              <img src="img_avatar2.png" alt="Avatar" class="avatar">
            </div>

            <div class="container" >
              <h1>Sign Up</h1>
              <p>Please fill in this form to create an account.</p>
              <hr>

              <label for="email"><b>Email</b></label><br>
              <input type="text" placeholder="Enter Email" name="email" required><br>

              <label for="psw"><b>Password</b></label><br>
              <input type="password" placeholder="Enter Password" name="psw" required><br>

              <label for="psw-repeat"><b>Repeat Password</b></label>
              <input type="password" placeholder="Repeat Password" name="psw-repeat" required>

              <label>
                <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> Remember me
              </label>

              <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>

              <div class="clearfix">
                <button type="button" class="cancelbtn">Cancel</button>
                <button type="submit" class="signupbtn">Sign Up</button>
              </div>
            </div>
          </div>
        </div>
      </form>
  </div>
<!--  -->

  <?php
  
    if ($content=='index')
      include 'slider.php';
    elseif ($content=='shop')
      include 'jumbotshop.php';
    elseif ($content=='service')
      include 'jumbotservice.php';
  
  ?>
  
  <div class="container">
    <?php

      if ($content=='index')
        include 'beranda.php';

      elseif ($content=='shop')
        include 'shop.php';
      elseif ($content=='detail_product')
        include 'detail_product.php';
      elseif ($content=='checkout')
        include 'checkout.php';
      
      elseif ($content=='service')
        include 'service.php';

      elseif ($content=='contact')
        include 'contact.php';

      elseif ($content=='detail_service')
        include 'detail_service.php';
      
    ?>
  </div>
  <br>
  <br>
  <footer class="container-fluid" style="background-color: #222">
    <p style="color:white">
      Copyright © 2018 HOLOMOC. All rights reserved.

      Need Help? <a href="index.php?content=contact">Contact Us</a>
    </p>
  </footer>
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
            <br>
            <br>
            <br>
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <ul class="nav nav-tabs">
                  <li class="active" style="background-color: #f2f3f4"><a data-toggle="tab" href="#login"><font color="black">LOGIN</font></a></li>
                  <li style="background-color: #f2f3f4"><a data-toggle="tab" href="#signup"><font color="black">SIGN UP</font></a></li>
                </ul>
              </div>
              <div class="modal-body">
                <div class="tab-content">
                  <div id="login" class="tab-pane fade in active">
                    <h4><center><b>SILAHKAN LOGIN</b></center></h4>
                    <br>
                    <form>
                      <div class="form-group">
                        <label for="username">Username:</label>
                        <input type="usernae" class="form-control" id="username" placeholder="Enter username">
                      </div>
                      <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input type="password" class="form-control" id="pwd" placeholder="Enter password">
                      </div>
                      <div class="col-sm-2">
                      </div>
                      <div class="col-sm-8">
                      </div>
                      <div class="col-sm-2">
                        <button type="submit" class="btn btn-default" style="background-color: #f2f3f4; color: black;">Login</button>
                      </div>
                    </form>
                  </div>
                  <div id="signup" class="tab-pane fade">
                    <h4><center><b>MEMBUAT AKUN</b></center></h4>
                    <br>
                    <form>
                      <div class="form-group">
                        <label for="username">Nama Lengkap:</label>
                        <input type="text" class="form-control" id="name" placeholder="Enter Your Name">
                      </div>
                      <div class="form-group">
                        <label for="pwd">Alamat:</label>
                        <textarea class="form-control" placeholder="Enter Your Address"></textarea>
                        <!-- <input type="password" class="form-control" id="pwd" placeholder="Enter password"> -->
                      </div>
                      <div class="form-group">
                        <label for="username">Alamat Email:</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter Your Email">
                      </div>
                      <div class="form-group">
                        <label for="username">No. Hp:</label>
                        <input type="text" class="form-control" placeholder="Enter Your Phone Number">
                      </div>
                      <div class="form-group">
                        <label for="username">Username:</label>
                        <input type="username" class="form-control" id="username" placeholder="Enter Your Username">
                      </div>
                      <div class="form-group">
                        <label for="username">Password:</label>
                        <input type="password" class="form-control" id="password" placeholder="Enter Your Password">
                      </div>
                      <div class="col-sm-2">
                      </div>
                      <div class="col-sm-8">
                      </div>
                      <div class="col-sm-2">
                        <button type="submit" class="btn btn-default" style="background-color: #f2f3f4; color: black;">Login</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="modal-footer">

              </div>
            </div>
    </div>
  </div>
</body>
</html>
<script type="text/javascript">


  $('.dropdown-toggle').click(function(e) {
    if ($(document).width() > 768) {
      e.preventDefault();

      var url = $(this).attr('href');
         
      if (url !== '#') {
      
        window.location.href = url;

      }
    }
  });
</script>