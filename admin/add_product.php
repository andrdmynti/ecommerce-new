<div class="main-content-inner">
	<div class="breadcrumbs ace-save-state" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
				<i class="ace-icon fa fa-home home-icon"></i>
				<a href="index.php">Home</a>
			</li>
			<li>
				<a href="index.php?content=manage_product">Management Product</a>
			</li>
			<li class="active">Create New Product</li>
		</ul><!-- /.breadcrumb -->

		<div class="nav-search" id="nav-search">
			<form class="form-search">
				<span class="input-icon">
					<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
					<i class="ace-icon fa fa-search nav-search-icon"></i>
				</span>
			</form>
		</div><!-- /.nav-search -->
	</div>
	<br>
	<div class="page-content">
		<div class="page-header">
			<h1>
				Create New Product
			</h1>
		</div><!-- /.page-header -->
		<br>
		<br>
		<div class="row">
			<div class="col-xs-12">
				<!-- PAGE CONTENT BEGINS -->
				<form class="form-horizontal" role="form" action="../action/doAddProduct.php" enctype="multipart/form-data" method="POST">
					<div class="form-group">
						<label class="col-sm-3" for="form-field-1">&nbsp;&nbsp;&nbsp;Product Code</label>
						<div class="col-sm-9">
							<input type="text" id="form-field-1" name="code_product" placeholder="Product Code" class="col-xs-10 col-sm-9" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3" for="form-field-1">&nbsp;&nbsp;&nbsp;Product Category</label>
						<div class="col-sm-9">
							<select class="col-xs-10 col-sm-9" name="category_product" placeholder="Choose Product Category">
								<option> ---Choose Product Category--- </option>
							    <?php
							      include '../action/connection.php';

							      $query 		= "SELECT * FROM product_category";
							      $insert	 	= mysqli_query($connect,$query);
							      while ($tampil = mysqli_fetch_array($insert)) { ?>
							         <option value="<?php echo $tampil['id'] ?>"><?php echo $tampil["category"] ?></option>
							    <?php
							    	}
							    ?>
							  </select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3" for="form-field-1">&nbsp;&nbsp;&nbsp;Product Name</label>
						<div class="col-sm-9">
							<input type="text" id="form-field-1" placeholder="Product Name" name="product_name" class="col-xs-10 col-sm-9" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3" for="form-field-1">&nbsp;&nbsp;&nbsp;Product Price</label>
						<div class="col-sm-9">
							<input type="text" id="form-field-1" placeholder="Product Price" name="price" class="col-xs-10 col-sm-9" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3" for="form-field-1">&nbsp;&nbsp;&nbsp;Product Image</label>
						<div class="col-sm-9">
							<input class="col-xs-10 col-sm-9" type="file" name="fileToUploadProduct" id="fileToUploadProduct">
						</div>
					</div>
					<hr>
					<h3>Product Detail</h3>
					<hr>
					<div class="form-group">
						<label class="col-sm-3" for="form-field-1">&nbsp;&nbsp;&nbsp;Product Description</label>
						<div class="col-sm-9">
							<textarea type="text" id="form-field-1" placeholder="Product Description" name="description" class="col-xs-10 col-sm-9" /></textarea> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3" for="form-field-1">&nbsp;&nbsp;&nbsp;Product Image 1</label>
						<div class="col-sm-9">
							<input class="col-xs-10 col-sm-9" type="file" name="fileToUploadOne" id="fileToUploadOne">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3" for="form-field-1">&nbsp;&nbsp;&nbsp;Product Image 2</label>
						<div class="col-sm-9">
							<input class="col-xs-10 col-sm-9" type="file" name="fileToUploadTwo" id="fileToUploadTwo">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3" for="form-field-1">&nbsp;&nbsp;&nbsp;Product Image 3</label>
						<div class="col-sm-9">
							<input class="col-xs-10 col-sm-9" type="file" name="fileToUploadThree" id="fileToUploadThree">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3" for="form-field-1">&nbsp;&nbsp;&nbsp;Product Image 4</label>
						<div class="col-sm-9">
							<input class="col-xs-10 col-sm-9" type="file" name="fileToUploadFour" id="fileToUploadFour">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10" id="default-buttons">
							<button type="submit" class="btn btn-primary btn-sm" style="float: right;">Save</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>