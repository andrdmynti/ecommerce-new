<br>
<br>
<br>
<br>
<br>
<br>
<div class="row">
  <div class="col-sm-8">
    <div class="breadcrumbs">
      <ol class="breadcrumb" style="background: white">
        <li><a href="index.php"><font color="black">Homepage</font></a></li>
        <li><a href="shop.php"><font color="black">Shop</font></a></li>
        <li><a href="index.php"><font color="black">Category</font></a></li>
        <li><a href="detail_product.php"><font color="black">Nama Product</font></a></li>
        <li class="active">Checkout</li>
      </ol>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-sm-8"  style="background-color:#f2f2f2;">
    <div class="col-sm-6" style="padding-bottom:20px;">
      <form action="/action_page.php">
        <h4>Detail Pemesan</h4>
        <br>
        <div class="form-group">
          <label class="col-sm-8"><i class="fa fa-user"></i> Nama Lengkap</label>
          <input class="col-sm-4 form-control" type="text" id="fname" name="name"> 
          <br> 
        </div>
        <div class="form-group">
          <label class="col-sm-8"><i class="fa fa-envelope"></i> Email</label>
          <input class="col-sm-4 form-control" type="email" id="email" name="email"> 
          <br>
        </div>
        <div class="form-group">
          <label class="col-sm-8"><i class="fa fa-list-alt"></i> Alamat</label>
          <textarea class="col-sm-4 form-control" name="alamat" type="text"></textarea>
        </div>
        <div class="form-group">
          <label class="col-sm-8"><i class="fa fa-institution"></i> Kode Pos</label>
          <input class="col-sm-4 form-control" type="text" id="pos" name="kodepos">
        </div>
        <div class="form-group">
          <label class="col-sm-8"><i class="fa fa-phone"></i> Nomor Handphone</label>
          <input class="col-sm-4 form-control" type="text" id="nomer" name="nohp">
        </div>
      </form>
    </div>
    <div class="col-sm-6" style="padding-bottom:20px;">
      <center><h4>Payment</h4></center>
      <br>
      <div class="row">
        <div class="col-sm-12">
          <center><img src="./images/logobca.png" style="width:60%;"></center>
          <center><label class="radio-inline"><input type="radio" name="payment" value="bca" checked>Transfer BCA</label></center>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-sm-12">
          <center><img src="./images/virtual.png" style="width:60%;"></center>
          <center><label class="radio-inline"><input type="radio" value="bcava" name="payment">BCA Virtual Account</label></center>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-4" style="padding-bottom:20px;">
    <div class="col-sm-12"  style="background-color:#f2f2f2">
      <h4>Cart
        <span class="price" style="color:black">
          <i class="fa fa-shopping-cart"></i>
          <b>4</b>
        </span>
      </h4>
      <div style="row">
        <img src="" style="width:60px; height:60px;">
        <p><a href="#">Product 1</a> <span class="price">$15</span></p>
      </div>
      <div style="row">
        <img src="" style="width:60px; height:60px;">
        <p><a href="#">Product 2</a> <span class="price">$5</span></p>
      </div>
      <div style="row">
        <img src="" style="width:60px; height:60px;">
        <p><a href="#">Product 3</a> <span class="price">$8</span></p>
      </div>
      <div style="row">
        <img src="" style="width:60px; height:60px;">
        <p><a href="#">Product 4</a> <span class="price">$2</span></p>
      </div>
      <hr>
      <p>Total <span class="price" style="color:black"><b>$30</b></span></p>
    </div>
    <div class="col-sm-12" style="background-color:#f2f2f2; padding-bottom:20px;">
      <input type="submit" value="Continue to checkout" class="btn" style="width:100%; font-color:#ffffff; background-color:#14a2dd;">
    </div>
</div>

