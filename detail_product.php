<br>
<br>
<br>
<br>
<br>
<br>
<div class="row">
  <div class="col-sm-8">
    <div class="breadcrumbs">
      <ol class="breadcrumb" style="background: white">
        <li><a href="index.php"><font color="black">Homepage</font></a></li>
        <li><a href="index.php"><font color="black">Shop</font></a></li>
        <li><a href="index.php"><font color="black">Category</font></a></li>
        <li class="active">Nama Product</li>
      </ol>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-4">
    <img src="">
  </div>
  <div class="col-sm-6">
    <h3><b>NAMA PRODUCT</b></h3>
    <br>
    Category : Nama Category
    <hr>
    <h1><font color="#2dcaff"><b>IDR 18.950.000</font></b></h1>
  </div>
  <div class="col-sm-2">
    <h2>
      <a href="" style="color: grey"><i class="fa fa-share-alt"></i></a>
    </h2>
  </div>
  
  <div class="col-sm-4">
    
  </div>
  <div class="col-sm-8">
    <h4><b>Descriptions</b></h4>
    <br>
    <div class="jumbotron" style="padding-top: 5px; padding-left: 25px"> 
      <h4>Bootstrap is the most popular HTML, CSS, and JS framework for developing
      responsive, mobile-first projects on the web.</h4> 
    </div>
    <br>
    <div class="col-sm-4">
      <h4><b>Quantity :</b></h4>      
    </div>
    <div class="col-sm-4">
      <form>
        <div class="input-group">
          <div class="input-group-btn">
            <button class="btn btn-default" type="button">
              <i class="glyphicon glyphicon-minus"></i>
            </button>
          </div>
          <input type="text" class="form-control" placeholder="Search" style="margin:0px;">
          <div class="input-group-btn">
            <button class="btn btn-default" type="button">
              <i class="glyphicon glyphicon-plus"></i>
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="col-sm-4">
    
  </div>
  <div class="col-sm-2">
    
  </div>
  <div class="col-sm-4">
    <br>
    <a href="index.php?content=checkout"><button type="button" class="btn btn-primary btn-block btn-lg" style="background-color: white;"><font color="black">Beli</font></button></a>
  </div>
  <div class="col-sm-2">
    
  </div>  
</div>
<br>
<br>
<ul class="nav nav-tabs">
  <li class="active" style="background-color: #f2f3f4"><a data-toggle="tab" href="#home"><font color="black">Detail Product</font></a></li>
  <li style="background-color: #f2f3f4"><a data-toggle="tab" href="#menu1"><font color="black">Features</font></a></li>
</ul>

<div class="tab-content" style="background-color: #f2f3f4; padding-top: 2px" >
  <div id="home" class="tab-pane fade in active" style="background-color: #f2f3f4">
    <div class="col-sm-0">
      
    </div>
    <div class="col-sm-10">
      <br>
      <h4><b>INFORMATION</b></h4>
      <p>
        <span class=""></span>
      </p>
      <hr>
      <h4><b>DESCRIPTION</b></h4>
      <p>
          
      </p>
      <hr>
      <h4><b>SPECIFICATION</b></h4>
      <p>
        
      </p>
      <hr>
      <h4><b>WARRANTY</b></h4>
      <p>
        
      </p>
      <hr>
      <h4><b>ACCESSORIES</b></h4>
      <p>
        
      </p>
    </div>
    <div class="col-sm-1">
      
    </div>
  </div>
  <div id="menu1" class="tab-pane fade" style="background-color: #f2f3f4">
    <div class="col-sm-0">
      
    </div>
    <div class="col-sm-10">
      <br>
      <h4><b>SPECIAL FEATURES</b></h4>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </p>
      <hr>
      <hr>
      <h4><b>SOFTWARE COMPATIBILITY</b></h4>
      <p>
        
      </p>
    </div>
    <div class="col-sm-1">
      
    </div>
  </div>
</div>