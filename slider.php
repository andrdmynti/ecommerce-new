
<br>
<br>
<br>
<br>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active" style="background-color: rgb(51, 51, 51);"></li>
    <li data-target="#myCarousel" data-slide-to="1" style="background-color: rgb(51, 51, 51);"></li>
    <li data-target="#myCarousel" data-slide-to="2" style="background-color: rgb(51, 51, 51);"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <!-- SLIDE PERTAMA -->
    <div class="item active">
      <div class="row">
        <div class="col-sm-12">
          <br>
          <br>
          <div class="col-sm-1">
            
          </div>
          <div class="col-sm-6">
            <br>
            <h1><font style="font-family: Abel; font-size: 40px; font-weight: 100;"><span style="font-color: rgb(39, 39, 39);">AUGMENTED</span></font><font color=#b4b1ab style="font-family: Abel; font-size: 40px; font-weight: 100;">REALITY</font></h1>
            <h2><font style="font-color: #363432; font-family: 'Roboto', sans-serif; font-size: 28px; font-weight: 700;">Creative Digital Technology</font></h2>
            <p><font style="color:#363432;font-family: 'Roboto', sans-serif; font-size: 16px; font-weight: 300">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </font></p>
            <br>
            <button type="button" class="btn btn-default">LEARN MORE</button>
          </div>
          <div class="col-sm-4">
            <img src="images/girl2.jpg" class="girl img-responsive" alt="" />
          </div>
          <div class="col-sm-1">
            
          </div>
        </div>
      </div>
    </div>

    <!-- SLIDE KEDUA -->
    <div class="item">
      <div class="row">
        <div class="col-sm-12">
          <br>
          <br>
          <div class="col-sm-1">
            
          </div>
          <div class="col-sm-6">
            <br>
            <h1><font style="font-family: Abel; font-size: 40px; font-weight: 100;"><span style="font-color: rgb(39, 39, 39);">DIGITAL</span></font><font color=#b4b1ab style="font-family: Abel; font-size: 40px; font-weight: 100;">BANNER</font></h1>
            <h2><font style="font-color: #363432; font-family: 'Roboto', sans-serif; font-size: 28px; font-weight: 700;">Digital Banner Holomoc</font></h2>
            <p><font style="color:#363432;font-family: 'Roboto', sans-serif; font-size: 16px; font-weight: 300">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </font></p>
            <br>
            <button type="button" class="btn btn-default">Get It Now</button>
          </div>
          <div class="col-sm-4">
            <img src="images/girl2.jpg" class="girl img-responsive" alt="" />
          </div>
          <div class="col-sm-1">
            
          </div>
        </div>
      </div>
    </div>

    <!-- SLIDE KETIGA -->
    <div class="item">
      <div class="row">
        <div class="col-sm-12">
          <br>
          <br>
          <div class="col-sm-1">
            
          </div>
          <div class="col-sm-6">
            <br>
            <h1><font style="font-family: Abel; font-size: 40px; font-weight: 100;"><span style="font-color: rgb(39, 39, 39);">WIVI</span></font><font color=#b4b1ab style="font-family: Abel; font-size: 40px; font-weight: 100;">TOUCH</font></h1>
            <h2><font style="font-color: #363432; font-family: 'Roboto', sans-serif; font-size: 28px; font-weight: 700;">AnotherCreative Digital Technology</font></h2>
            <p><font style="color:#363432;font-family: 'Roboto', sans-serif; font-size: 16px; font-weight: 300">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </font></p>
            <br>
            <button type="button" class="btn btn-default">Get It Now!</button>
          </div>
          <div class="col-sm-4">
            <img src="images/girl2.jpg" class="girl img-responsive" alt="" />
          </div>
          <div class="col-sm-1">
            
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<br>