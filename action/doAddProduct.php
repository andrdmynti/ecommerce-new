<?php
	
	include 'connection.php';

	//INSERT INTO PRODUCT

	$code 		= $_POST['code_product'];
	$category 	= $_POST['category_product'];
	$name	 	= $_POST['product_name'];
	$price	 	= $_POST['price'];

	$target_dirProduct  = "../images/product_upload/";
	$target_fileProduct = $target_dirProduct . basename($_FILES["fileToUploadProduct"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_fileProduct,PATHINFO_EXTENSION);
	
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
	    $check = getimagesize($_FILES["fileToUploadProduct"]["tmp_name"]);
	    if($check !== false) {
	        echo "File is an image - " . $check["mime"] . ".";
	        $uploadOk = 1;
	    } else {
	        echo "File is not an image.";
	        $uploadOk = 0;
	    }
	}
	
	// Check file size
	if ($_FILES["fileToUploadProduct"]["size"] > 1000000000) {
	    echo "Sorry, your file is too large.";
	    $uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType != "JPEG"
	&& $imageFileType != "GIF" ) {
	    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
	    $uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
	    echo "Sorry, your file was not uploaded.";
	
	// if everything is ok, try to upload file
	} else {
	    if (move_uploaded_file($_FILES["fileToUploadProduct"]["tmp_name"], $target_fileProduct)) {
	        echo "The file ". basename( $_FILES["fileToUploadProduct"]["name"]). " has been uploaded.";
	    } else {
	        echo "Sorry, there was an error uploading your file.";
	    }
	}

	$query  	= "INSERT INTO product (id,product_code,id_category_product,product_name,product_image,product_price) VALUES ('','$code','$category','$name','$target_fileProduct','$price')";
	$simpan		= mysqli_query($connect, $query)or die(mysqli_error($connect));

	

	if ($simpan){
		//INSERT INTO PRODUCT DETAIL

		$description	= $_POST['description'];

		//detail image one
		$target_dirOne  = "../images/product_upload/";
		$target_fileOne = $target_dirOne . basename($_FILES["fileToUploadOne"]["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_fileOne,PATHINFO_EXTENSION);
		
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["fileToUploadOne"]["tmp_name"]);
		    if($check !== false) {
		        echo "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        echo "File is not an image.";
		        $uploadOk = 0;
		    }
		}
		
		// Check file size
		if ($_FILES["fileToUploadOne"]["size"] > 1000000000) {
		    echo "Sorry, your file is too large.";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType != "JPEG"
		&& $imageFileType != "GIF" ) {
		    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    echo "Sorry, your file was not uploaded.";
		
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["fileToUploadOne"]["tmp_name"], $target_fileProduct)) {
		        echo "The file ". basename( $_FILES["fileToUploadOne"]["name"]). " has been uploaded.";
		    } else {
		        echo "Sorry, there was an error uploading your file.";
		    }
		}

		//detail image two
		$target_dirTwo  = "../images/product_upload/";
		$target_fileTwo = $target_dirTwo . basename($_FILES["fileToUploadTwo"]["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_fileTwo,PATHINFO_EXTENSION);
		
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["fileToUploadTwo"]["tmp_name"]);
		    if($check !== false) {
		        echo "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        echo "File is not an image.";
		        $uploadOk = 0;
		    }
		}
		
		// Check file size
		if ($_FILES["fileToUploadTwo"]["size"] > 1000000000) {
		    echo "Sorry, your file is too large.";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType != "JPEG"
		&& $imageFileType != "GIF" ) {
		    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    echo "Sorry, your file was not uploaded.";
		
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["fileToUploadTwo"]["tmp_name"], $target_fileProduct)) {
		        echo "The file ". basename( $_FILES["fileToUploadTwo"]["name"]). " has been uploaded.";
		    } else {
		        echo "Sorry, there was an error uploading your file.";
		    }
		}

		//DETAIL IMAGE THREE
		$target_dirThree  = "../images/product_upload/";
		$target_fileThree = $target_dirThree . basename($_FILES["fileToUploadThree"]["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_fileThree,PATHINFO_EXTENSION);
		
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["fileToUploadThree"]["tmp_name"]);
		    if($check !== false) {
		        echo "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        echo "File is not an image.";
		        $uploadOk = 0;
		    }
		}
		
		// Check file size
		if ($_FILES["fileToUploadThree"]["size"] > 1000000000) {
		    echo "Sorry, your file is too large.";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType != "JPEG"
		&& $imageFileType != "GIF" ) {
		    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    echo "Sorry, your file was not uploaded.";
		
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["fileToUploadThree"]["tmp_name"], $target_fileProduct)) {
		        echo "The file ". basename( $_FILES["fileToUploadThree"]["name"]). " has been uploaded.";
		    } else {
		        echo "Sorry, there was an error uploading your file.";
		    }
		}


		//DETAIL IMAGE THREE
		$target_dirFour  = "../images/product_upload/";
		$target_fileFour = $target_dirFour . basename($_FILES["fileToUploadFour"]["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_fileFour,PATHINFO_EXTENSION);
		
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["fileToUploadFour"]["tmp_name"]);
		    if($check !== false) {
		        echo "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        echo "File is not an image.";
		        $uploadOk = 0;
		    }
		}
		
		// Check file size
		if ($_FILES["fileToUploadFour"]["size"] > 1000000000) {
		    echo "Sorry, your file is too large.";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType != "JPEG"
		&& $imageFileType != "GIF" ) {
		    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    echo "Sorry, your file was not uploaded.";
		
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["fileToUploadFour"]["tmp_name"], $target_fileProduct)) {
		        echo "The file ". basename( $_FILES["fileToUploadFour"]["name"]). " has been uploaded.";
		    } else {
		        echo "Sorry, there was an error uploading your file.";
		    }
		}


		$queryTampil 	= "SELECT id FROM product ORDER BY id DESC LIMIT 1";
		$hasil   		= mysqli_query($connect, $queryTampil)or die(mysqli_error($connect));
		$data    		= mysqli_fetch_array($hasil);

		$id_product 	= $data['id'];
		//echo $id_product;

		$query  	= "INSERT INTO product_detail (id,id_product,id_category,product_image_1,product_image_2,product_image_3,product_image_4,product_description) VALUES ('','$id_product','$category','$target_fileOne','$target_fileTwo','$target_fileThree','$target_fileFour', '$description')";
		$simpan		= mysqli_query($connect, $query)or die(mysqli_error($connect));

	}

	// echo "<br><br><br><strong><center><i>Anda berhasil menambahkan produk!";
	echo '<META HTTP-EQUIV="REFRESH" CONTENT = "1; URL=../admin/index.php?content=manage_product">';

?>