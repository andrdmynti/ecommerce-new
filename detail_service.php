<br>
<br>
<br>
<br>
<br>
<br>
<div class="row">
  <div class="col-sm-8">
    <div class="breadcrumbs">
      <ol class="breadcrumb" style="background: white">
        <li><a href="index.php"><font color="black">Homepage</font></a></li>
        <li><a href="service.php"><font color="black">Service</font></a></li>
        <li><a href="index.php"><font color="black">Category</font></a></li>
        <li class="active">Nama Product</li>
      </ol>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <img src="">
  </div>
  <div class="col-sm-5">
    <h3><b>NAMA PRODUCT</b></h3>
    <br>
    Category : Nama Category
    <hr>
  </div>
  <div class="col-sm-1">
    <center><h2>
      <a href="" style="color: grey"><i class="fa fa-share-alt"></i></a>
    </h2></center>
  </div>
  
  <div class="col-sm-6">
    
  </div>
  <div class="col-sm-5">
    <h4><b>Descriptions</b></h4>
    <br>
    <div class="jumbotron" style="padding-top: 5px; padding-left: 25px"> 
      <h4>Bootstrap is the most popular HTML, CSS, and JS framework for developing
      responsive, mobile-first projects on the web.</h4> 
    </div>
    <br>
    <hr>
  </div>
  <div class="col-sm-4">
    
  </div>
  <div class="col-sm-2">
    
  </div>
  <div class="col-sm-5">
    <br>
    <a href="index.php?content=checkout"><button type="button" class="btn btn-primary btn-block btn-lg" style="background-color: white;"><font color="black">Pesan Sekarang</font></button></a>
  </div>
  <div class="col-sm-2">
    
  </div>  
</div>
<br>
<br>
<hr>
<div class="row">
  <div class="col-sm-12">
    <center><h2>FEATURES</h2></center>
  </div>
  <div class="col-sm-4" style="padding-bottom:30px;">
    <div class="col-sm-4">
      <img src="./images/responsive.png" style="width:100%;">
    </div>
    <div class="col-sm-8">
      <h3>Responsive Design</h3>
      <p>Looks great on all devices</p>
    </div>
  </div>
  <div class="col-sm-4" style="padding-bottom:30px;">
    <div class="col-sm-4">
      <img src="./images/socialmedia.png" style="width:100%;">
    </div>
    <div class="col-sm-8">
      <h3>Social Media</h3>
      <p>Integrate all your accounts</p>
    </div>
  </div>
  <div class="col-sm-4" style="padding-bottom:30px;">
    <div class="col-sm-4">
     <img src="./images/seo.png" style="width:100%;">
    </div>
    <h3>SEO</h3>
    <p>Optimized for search engines</p>
  </div>
  <div class="col-sm-4" style="padding-bottom:30px;">
    <div class="col-sm-4">
      <img src="./images/plugins.png" style="width:100%;">
    </div>
    <div class="col-sm-8">
      <h3>Plugins</h3>
      <p>Supercharge your website</p>
    </div>
  </div>
  <div class="col-sm-4" style="padding-bottom:30px;">
    <div class="col-sm-4">
      <img src="./images/ecommerce.png" style="width:100%;">
    </div>
    <div class="col-sm-8">
      <h3>eCommerce</h3>
      <p>Sell your products or services online</p>
    </div>
  </div>
  <div class="col-sm-4" style="padding-bottom:30px;">
    <div class="col-sm-4">
     <img src="./images/blog.png" style="width:100%;">
    </div>
    <div class="col-sm-8">
      <h3>Blog</h3>
      <p>Tell the world your story</p>
    </div>
  </div>
  <div class="col-sm-4" style="padding-bottom:30px;">
    <div class="col-sm-4">
     <img src="./images/pages.png" style="width:100%;">
    </div>
    <div class="col-sm-4">
      <h3>Pages</h3>
      <p>Showcase your products and services</p>
    </div>
  </div>
  <div class="col-sm-4" style="padding-bottom:30px;">
    <div class="col-sm-4">
     <img src="./images/forms.png" style="width:100%;">
    </div>
    <div class="col-sm-8">
      <h3>Contact Forms</h3>
      <p>Generate enquiries and get feedback</p>
    </div>
  </div>
  <div class="col-sm-4" style="padding-bottom:30px;">
    <div class="col-sm-4">
     <img src="./images/pagebuilder.png" style="width:100%;">
    </div>
    <h3>Page builder</h3>
    <p>Build Impressive landing page</p>
  </div>
  <div class="col-sm-4" style="padding-bottom:30px;">
    <div class="col-sm-4">
     <img src="./images/porto.png" style="width:100%;">
    </div>
    <h3>Portofolio</h3>
    <p>showcase your beautifull work</p>
  </div>
</div>
